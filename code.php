<?php 
	$products =[];

	function createProduct(&$productArray, $productName, $productPrice){
		$productArray[] = [
			'Name' => $productName,
			'Price' => $productPrice
		];
	}

	function printProducts($productList){
		foreach($productList as $keys => $product){
			echo "<h2> $keys</h2>";
			foreach($product as $keys2 => $product2){
				echo "<li>$keys2 : $product2 <br/></li>";
			}
		}
	}

	function countProducts($productList){
		$arrLength = count($productList);
		echo "No. of Products: $arrLength";
	}

	function deleteProduct(&$productList){		
		array_pop($productList);
	}
?>