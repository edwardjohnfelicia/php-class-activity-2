<?php require "./code.php"; ?>
<!DOCTYPE html>
<html>
<head>
	<title>Array Manipulation</title>
</head>
<body>
	<h3>Create Product!</h3>
	<?php createProduct($products,"product1", 100); ?>
	<?php createProduct($products,"product2", 200); ?>
	<?php createProduct($products,"product3", 300); ?>
	<p><?php print_r($products); ?></p>
	<ul><?php printProducts($products); ?></ul>
	<span><?php countProducts($products); ?></span>
	<span><?php deleteProduct($products); ?></span>
	<p><?php print_r($products); ?></p>
</body>
</html>